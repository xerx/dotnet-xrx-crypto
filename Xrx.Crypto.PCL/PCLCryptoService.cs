﻿using PCLCrypto;
using System;
using System.Text;
using System.Threading.Tasks;
using Xrx.Crypto.Abstractions;
using Xrx.Crypto.Helpers;
using static PCLCrypto.WinRTCrypto;

namespace Xrx.Crypto.PCL
{
    public class PCLCryptoService : ICryptoService
    {
        public Task<string> AesEncrypt(string text, string key, string iv = null)
        {
            return Task.Factory.StartNew(() =>
            {
                ISymmetricKeyAlgorithmProvider provider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
                if (key.Length < provider.BlockLength)
                {
                    throw new ArgumentException($"Key should be {provider.BlockLength} bytes long");
                }
                byte[] keyBytes = Encoding.ASCII.GetBytes(key);
                byte[] textBytes = Encoding.ASCII.GetBytes(text);

                ICryptographicKey cryptoKey = provider.CreateSymmetricKey(keyBytes);

                byte[] ivBytes = iv == null ? null : Encoding.ASCII.GetBytes(iv);
                byte[] cipherBytes = CryptographicEngine.Encrypt(cryptoKey, textBytes, ivBytes);
                return CryptoHelper.BytesToByteCodeString(cipherBytes);
            });
        }
        public Task<string> AesDecrypt(string cipherText, string key, string iv = null)
        {
            return Task.Factory.StartNew(() =>
            {
                ISymmetricKeyAlgorithmProvider provider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
                byte[] keyBytes = Encoding.ASCII.GetBytes(key);
                ICryptographicKey cryptoKey = provider.CreateSymmetricKey(keyBytes);

                byte[] cipherBytes = CryptoHelper.ByteCodeStringToBytes(cipherText);
                byte[] ivBytes = iv == null ? null : Encoding.ASCII.GetBytes(iv);
                byte[] textBytes = CryptographicEngine.Decrypt(cryptoKey, cipherBytes, ivBytes);
                return Encoding.ASCII.GetString(textBytes);
            });
        }

        public Task<string> SHA256(string text, ISalt salt)
        {
            return Task.Factory.StartNew(() =>
            {
                IHashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha256);
                return SHA(hasher, salt.ApplyTo(text));
            });
        }
        public Task<string> SHA384(string text, ISalt salt)
        {
            return Task.Factory.StartNew(() =>
            {
                string saltText = salt.ApplyTo(text);
                IHashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha384);
                return SHA(hasher, saltText);
            });
        }
        private string SHA(IHashAlgorithmProvider hasher, string saltedText)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(saltedText);
            byte[] byteHash = hasher.HashData(bytes);
            return CryptoHelper.BytesToByteCodeString(byteHash);
        }

        public string GetRandomAesKey()
        {
            ISymmetricKeyAlgorithmProvider provider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            byte[] randomBytes = CryptographicBuffer.GenerateRandom(provider.BlockLength);
            return CryptoHelper.BytesToByteCodeString(randomBytes);
        }

        public string GetRandomAesIV()
        {
            ISymmetricKeyAlgorithmProvider provider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            byte[] randomBytes = CryptographicBuffer.GenerateRandom(provider.BlockLength >> 1);
            return CryptoHelper.BytesToByteCodeString(randomBytes);
        }
    }
}
