﻿using System;
using Shouldly;
using Xrx.Crypto.Abstractions;
using Xrx.Crypto.Salting;
using Xunit;

namespace Xrx.Crypto.PCL.Tests
{
    public class PCLCryptoServiceTests
    {
        [Fact]
        public void GetRandomAesKey_ShouldHaveTheRightLength()
        {
            int expected = 32;
            PCLCryptoService service = new PCLCryptoService();
            int actual = service.GetRandomAesKey().Length;
            actual.ShouldBe(expected);
        }

        [Theory]
        [InlineData("password", "salt", "7a37b85c8918eac19a9089c0fa5a2ab4dce3f90528dcdeec108b23ddf3607b99")]
        [InlineData("!3efF$6&SJs", "_longersalt_", "0a5a1c71dce2ddd9cd15a4f6eef4a802e4318b4102d8827191cc226393cdc8dd")]
        public void SHA256_ShouldReturnValidHash(string text, string saltText, string expected)
        {
            PCLCryptoService service = new PCLCryptoService();
            ISalt salt = new Salt(new SaltingEnd(), saltText);
            string actual = service.SHA256(text, salt).GetAwaiter().GetResult();
            actual.ShouldBe(expected);
        }
        [Theory]
        [InlineData("c1d1ab650413a76dfd9878223a110a77", "fe59e54ec4dbf50f1dfc0d2624e851ee", "password")]
        public void AesDecrypt_ShouldReturnCorrectString(string cipherText, string key, string expected)
        {
            PCLCryptoService service = new PCLCryptoService();
            string actual = service.AesDecrypt(cipherText, key).GetAwaiter().GetResult();
            actual.ShouldBe(expected);
        }
    }
}