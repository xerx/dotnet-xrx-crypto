﻿using System;
using Xrx.Crypto.Salting;
using Xrx.Crypto.Abstractions;
using Xrx.Crypto.PCL;
using System.Threading.Tasks;

namespace Playground
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var ss = "12345678901234567890123456";
            var s = new Salt(new SaltingEnd(), ss, 
                                    new Salt(new SaltingStart(), ss,
                                                    new Salt(new SaltingMiddle(), ss)));
            var s2 = new Salt(new SaltingEnd(), ss);
            Console.WriteLine(s.ApplyTo("text"));
            ICryptoService sss = new PCLCryptoService();

            Console.WriteLine(sss.SHA256("hello", s2).GetAwaiter().GetResult());

            string k = sss.GetRandomAesKey();
            Console.WriteLine("Key:" + k);
            string encr = sss.AesEncrypt("password", k).GetAwaiter().GetResult();
            Console.WriteLine("Encr:"+encr);
            string decr = sss.AesDecrypt(encr, k).GetAwaiter().GetResult();
            Console.WriteLine("DEncr:" + decr);
            Console.ReadLine();
        }


    }
}
