﻿using System;
using System.Text;

namespace Xrx.Crypto.Helpers
{
    public static class CryptoHelper
    {
        public static string BytesToByteCodeString(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }
        public static byte[] ByteCodeStringToBytes(string text)
        {
            byte[] bytes = new byte[text.Length >> 1];
            for (int i = 0; i < text.Length; i += 2)
            {
                bytes[i >> 1] = Convert.ToByte(text.Substring(i, 2), 16);
            }
            return bytes;
        }
        private static readonly string charactersPool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        public static string GetRandomString(int len)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            int poolMax = charactersPool.Length - 1;
            for (int i = 0; i < len; i++)
            {
                builder.Append(charactersPool[random.Next(0, poolMax)]);
            }
            return builder.ToString();
        }
    }
}
