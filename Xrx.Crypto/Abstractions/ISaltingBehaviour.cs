﻿using System;
namespace Xrx.Crypto.Abstractions
{
    public interface ISaltingBehaviour
    {
        string ApplySalt(string text, string salt);
    }
}
