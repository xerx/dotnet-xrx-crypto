﻿using System;
namespace Xrx.Crypto.Abstractions
{
    public interface ISalt
    {
        string ApplyTo(string text);
        string Text { get; }
    }
}
