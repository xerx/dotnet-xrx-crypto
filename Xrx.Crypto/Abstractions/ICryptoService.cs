﻿using System.Threading.Tasks;

namespace Xrx.Crypto.Abstractions
{
    public interface ICryptoService
    {
        Task<string> AesEncrypt(string text, string key, string iv = null);
        Task<string> AesDecrypt(string cipherText, string key, string iv = null);
        Task<string> SHA256(string text, ISalt saltBuilder);
        Task<string> SHA384(string text, ISalt saltBuilder);
        string GetRandomAesKey();
        string GetRandomAesIV();
    }
}
