﻿using System;
using Xrx.Crypto.Abstractions;

namespace Xrx.Crypto.Salting
{
    public class Salt : ISalt
    {
        protected ISaltingBehaviour _behaviour;
        protected string _saltText;
        protected ISalt _decoratedSalt;
        public Salt(ISaltingBehaviour behaviour, string saltText, ISalt decoratedSalt = null)
        {
            _behaviour = behaviour;
            _saltText = saltText;
            _decoratedSalt = decoratedSalt;
        }

        public string Text => _saltText ?? "";

        public virtual string ApplyTo(string text)
        {
            if (_decoratedSalt != null)
            {
                text = _decoratedSalt.ApplyTo(text);
            }
            return _behaviour.ApplySalt(text, _saltText);
        }
    }
}
