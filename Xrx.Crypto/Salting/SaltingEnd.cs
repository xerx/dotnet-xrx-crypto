﻿using System;
using Xrx.Crypto.Abstractions;
namespace Xrx.Crypto.Salting
{
    public class SaltingEnd : ISaltingBehaviour
    {
        public string ApplySalt(string text, string salt) => text + salt;
    }
}
