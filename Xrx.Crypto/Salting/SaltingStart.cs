﻿using System;
using Xrx.Crypto.Abstractions;
namespace Xrx.Crypto.Salting
{
    public class SaltingStart : ISaltingBehaviour
    {
        public string ApplySalt(string text, string salt) => salt + text;
    }
}
