﻿using System;
using Xrx.Crypto.Abstractions;
namespace Xrx.Crypto.Salting
{
    public class SaltingMiddle : ISaltingBehaviour
    {
        public string ApplySalt(string text, string salt)
        {
            return text.Insert(text.Length >> 1, salt);
        }
    }
}
